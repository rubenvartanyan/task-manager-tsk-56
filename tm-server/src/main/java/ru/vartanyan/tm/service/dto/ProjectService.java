package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.dto.IProjectRepository;
import ru.vartanyan.tm.api.repository.dto.ISessionRepository;
import ru.vartanyan.tm.api.repository.dto.ITaskRepository;
import ru.vartanyan.tm.api.repository.dto.IUserRepository;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.repository.dto.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements
        IProjectService {

    @NotNull
    public IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.add(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyLoginException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.add(project);
            projectRepository.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            entities.forEach(projectRepository::add);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.clear();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(entity.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            return projectRepository.findAll();
        }
        catch (@NotNull final Exception e) {
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(
            @Nullable final String id
    ) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        if (id == null) throw new EmptyIdException();
        try {
            return projectRepository.findOneById(id);
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.clearByUserId(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            return projectRepository.findAllByUserId(userId);
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public @Nullable Project findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            return projectRepository.findOneByIdAndUserId(userId, id);
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public @Nullable Project findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            return projectRepository.findOneByName(userId, name);
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final Project entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByIdAndUserId(userId, entity.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByIdAndUserId(userId, id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            @NotNull Project project = projectRepository.findOneByIndex(userId, index);
            getProjectRepository().removeOneByIdAndUserId(userId, project.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByName(userId, name);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final Project entity = findOneById(userId, id);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final Project entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Project entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Project entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setStatus(Status.COMPLETE);
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Project entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setStatus(Status.IN_PROGRESS);
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final Project entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setStatus(Status.IN_PROGRESS);
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Project entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setName(name);
        entity.setDescription(description);
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == 0 || index == null || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setName(name);
        entity.setDescription(description);
        try {
            projectRepository.begin();
            projectRepository.update(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

}
