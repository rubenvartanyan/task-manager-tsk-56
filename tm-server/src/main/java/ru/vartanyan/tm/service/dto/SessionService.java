package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.dto.ISessionRepository;
import ru.vartanyan.tm.api.service.dto.ISessionService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.exception.system.UserLockedException;
import ru.vartanyan.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionService extends AbstractService<Session>
        implements ISessionService {

    @NotNull
    public ISessionRepository getSessionRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService PropertyService;
    
    @Override
    @SneakyThrows
    public void add(@Nullable final Session session) {
        if (session == null) throw new NullObjectException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(session);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @SneakyThrows
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || password == null) return false;
        final @NotNull User user = userService.findByLogin(login);
        if (user.getLocked()) throw new UserLockedException();
        final String passwordHash = HashUtil.salt(PropertyService, password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull User user = userService.findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final Session signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(signSession);
            sessionRepository.commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if ((session.getSignature() == null)) throw new AccessDeniedException();
        if ((session.getUserId()) == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        if (sessionRepository.findOneById(session.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final Session session,
                              @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        validate(session);
        final @NotNull User user = userService.findOneById(session.getUserId());
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    public Session close(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.removeOneById(session.getId());
            sessionRepository.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Nullable
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(PropertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Session> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            entities.forEach(sessionRepository::add);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.clear();
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Session entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.removeOneById(entity.getId());
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            return sessionRepository.findAll();
        } finally {
            sessionRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            return sessionRepository.findOneById(id);
        } finally {
            sessionRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.removeOneById(id);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

}

