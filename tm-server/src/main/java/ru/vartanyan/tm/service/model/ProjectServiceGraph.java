package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.dto.IUserRepository;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.ProjectGraph;
import ru.vartanyan.tm.repository.model.ProjectRepositoryGraph;
import ru.vartanyan.tm.repository.model.UserRepositoryGraph;


import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class ProjectServiceGraph extends AbstractServiceGraph<ProjectGraph>
        implements IProjectServiceGraph {

    @NotNull
    public IProjectRepositoryGraph getProjectRepositoryGraph() {
            return context.getBean(IProjectRepositoryGraph.class);
    }

    @NotNull
    public IUserRepositoryGraph getUserRepositoryGraph() {
        return context.getBean(IUserRepositoryGraph.class);
    }


    @Override
    @SneakyThrows
    public void add(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.add(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public ProjectGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            project.setUser(userRepositoryGraph.findOneById(userId));
            projectRepositoryGraph.begin();
            projectRepositoryGraph.add(project);
            projectRepositoryGraph.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            entities.forEach(projectRepositoryGraph::add);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.clear();
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.removeOneById(entity.getId());
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable List<ProjectGraph> findAll() {
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            return projectRepositoryGraph.findAll();
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findOneById(
            @Nullable final String id
    ) {
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        if (id == null) throw new EmptyIdException();
        try {
            return projectRepositoryGraph.findOneById(id);
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.removeOneById(id);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.clearByUserId(userId);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @NotNull List<ProjectGraph> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            return projectRepositoryGraph.findAllByUserId(userId);
        } finally {
            projectRepositoryGraph.close();
        }
    }


    @SneakyThrows
    @Override
    public @Nullable ProjectGraph findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            return projectRepositoryGraph.findOneByIdAndUserId(userId, id);
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @Nullable ProjectGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            return projectRepositoryGraph.findOneByIndex(userId, index);
        } finally {
            projectRepositoryGraph.close();
        }

    }

    @SneakyThrows
    @Override
    public @Nullable ProjectGraph findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            return projectRepositoryGraph.findOneByName(userId, name);
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final ProjectGraph entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.removeOneByIdAndUserId(userId, entity.getId());
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId, 
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.removeOneByIdAndUserId(userId, id);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId, 
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            @NotNull ProjectGraph project = projectRepositoryGraph.findOneByIndex(userId, index);
            projectRepositoryGraph.removeOneByIdAndUserId(userId, project.getId());
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.removeOneByName(userId, name);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setStatus(status);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(status);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByName(userId, name);
        entity.setStatus(status);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.COMPLETE);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final ProjectGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        try {
            projectRepositoryGraph.begin();
            projectRepositoryGraph.update(entity);
            projectRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            projectRepositoryGraph.close();
        }
    }

}
