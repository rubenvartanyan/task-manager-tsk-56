package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IProjectTaskServiceGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.repository.model.ProjectRepositoryGraph;
import ru.vartanyan.tm.repository.model.TaskRepositoryGraph;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class ProjectTaskServiceGraph implements IProjectTaskServiceGraph {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    public IProjectRepositoryGraph getProjectRepositoryGraph() {
        return context.getBean(IProjectRepositoryGraph.class);
    }

    @NotNull
    public ITaskRepositoryGraph getTaskRepositoryGraph() {
        return context.getBean(ITaskRepositoryGraph.class);
    }

    @SneakyThrows
    @Override
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.bindTaskByProjectId(userId, projectId, taskId);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @NotNull List<TaskGraph> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            return taskRepositoryGraph.findAllByProjectId(userId, projectId);
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            projectRepositoryGraph.begin();
            taskRepositoryGraph.removeAllByProjectId(userId, projectId);
            projectRepositoryGraph.removeOneByIdAndUserId(userId, projectId);
            projectRepositoryGraph.commit();
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            projectRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
            projectRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

}
