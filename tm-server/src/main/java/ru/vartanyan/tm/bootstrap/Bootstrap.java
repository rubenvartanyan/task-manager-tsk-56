package ru.vartanyan.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.endpoint.*;
import ru.vartanyan.tm.api.service.*;

import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.api.service.model.*;
import ru.vartanyan.tm.endpoint.*;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.service.dto.*;
import ru.vartanyan.tm.service.model.ProjectServiceGraph;
import ru.vartanyan.tm.service.model.ProjectTaskServiceGraph;
import ru.vartanyan.tm.service.model.SessionServiceGraph;
import ru.vartanyan.tm.service.model.TaskServiceGraph;
import ru.vartanyan.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    AbstractEndpoint[] abstractEndpoints;

    @NotNull
    public ILoggerService loggerService  = new LoggerService();;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    public IUserServiceGraph userService;

    @NotNull
    public IActiveMQConnectionService activeMQConnectionService;

    @Nullable
    private Session session = null;

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        Arrays.stream(abstractEndpoints).forEach(this::registry);
    }

    public void init() throws Exception {
        initPID();
        initEndpoint();
        initUser();
    }

    public void initUser() throws Exception {
        userService.create("test", "test");
        userService.create("admin", "admin", Role.ADMIN);
    }

}
