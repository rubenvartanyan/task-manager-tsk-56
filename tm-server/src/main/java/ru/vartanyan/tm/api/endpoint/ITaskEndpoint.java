package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    Task addTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException;

    @WebMethod
    void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws AccessDeniedException;

    @WebMethod
    void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException;

    void finishTaskById(
            @NotNull Session session,
            @NotNull String id
    ) throws AccessDeniedException;

    void finishTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index
    ) throws AccessDeniedException;

    void finishTaskByName(
            @NotNull Session session,
            @NotNull String name
    ) throws AccessDeniedException;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws AccessDeniedException;

    @WebMethod
    void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException;

    @WebMethod
    void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException;

    @WebMethod
    void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException;

    void startTaskById(
            @NotNull Session session,
            @NotNull String id
    ) throws AccessDeniedException;

    void startTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index
    ) throws AccessDeniedException;

    void startTaskByName(
            @NotNull Session session,
            @NotNull String name
    ) throws AccessDeniedException;

    @WebMethod
    void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException;

    void updateTaskById(
            @NotNull Session session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException;

    void updateTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException;

}
