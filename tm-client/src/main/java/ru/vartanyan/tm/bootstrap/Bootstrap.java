package ru.vartanyan.tm.bootstrap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.api.repository.*;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.command.system.ExitCommand;
import ru.vartanyan.tm.component.FileScanner;
import ru.vartanyan.tm.endpoint.*;
import ru.vartanyan.tm.repository.*;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.SystemUtil;
import ru.vartanyan.tm.util.TerminalUtil;
import sun.rmi.transport.Endpoint;

import java.io.File;
import java.lang.Exception;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    public ICommandService commandService;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

    @Autowired
    public AbstractCommand[] commands;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    private SessionDTO session = null;

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initCommands() {
        Arrays.stream(commands).forEach(cmd -> commandService.add(cmd));
    }

    public void process() {
        while (true){
            System.out.println();
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        @NotNull final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        @NotNull final AbstractCommand command = commandService.getCommandByName(arg);
        if (command == null) showIncorrectArg();
        else command.execute();
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) {
            showIncorrectCommand();
            return;
        }
        command.execute();
    }

    private void init() {
        initPID();
        initCommands();
        fileScanner.init();
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    public void run(final String... args) throws Exception {
        init();
        if (parseArgs(args)) new ExitCommand().execute();
        process();
    }

}
