package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.ProjectEndpoint;
import ru.vartanyan.tm.endpoint.TaskEndpoint;

public abstract class AbstractProjectCommand extends AbstractCommand{

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

}
