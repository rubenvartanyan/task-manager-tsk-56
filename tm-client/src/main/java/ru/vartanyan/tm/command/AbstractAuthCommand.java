package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.AdminEndpoint;
import ru.vartanyan.tm.endpoint.SessionEndpoint;

public abstract class AbstractAuthCommand extends AbstractCommand{

    @NotNull
    @Autowired
    public SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    public AdminEndpoint adminEndpoint;

}
