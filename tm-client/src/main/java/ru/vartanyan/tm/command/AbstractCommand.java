package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.endpoint.SessionDTO;

public abstract class AbstractCommand {

    @NotNull protected Bootstrap bootstrap;

    public AbstractCommand() {
    }

    public void setBootstrap(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable public abstract String arg();

    @NotNull public abstract String name();

    @Nullable public abstract String description();

    public abstract void execute() throws Exception;

}
