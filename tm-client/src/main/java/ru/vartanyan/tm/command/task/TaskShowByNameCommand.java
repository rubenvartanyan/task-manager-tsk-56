package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.endpoint.Task;
import ru.vartanyan.tm.endpoint.TaskDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-show-by-name";
    }

    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        @Nullable final SessionDTO session = bootstrap.getSession();
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.findTaskByName(name, session);
        System.out.println(task.getName() + ": " + task.getDescription());
    }

}
