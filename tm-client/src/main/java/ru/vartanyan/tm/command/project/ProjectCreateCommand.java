package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-create";
    }

    @Override
    public @Nullable String description() {
        return "Create project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull String name = TerminalUtil.nextLine();
        if ((name).isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull String description = TerminalUtil.nextLine();
        if ((description).isEmpty()) throw new EmptyDescriptionException();
        projectEndpoint.addProject(name, description, session);
        System.out.println("[PROJECT CREATED]");
    }
}
