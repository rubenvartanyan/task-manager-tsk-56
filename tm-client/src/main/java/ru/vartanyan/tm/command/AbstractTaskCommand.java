package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.AdminEndpoint;
import ru.vartanyan.tm.endpoint.TaskEndpoint;

public abstract class AbstractTaskCommand extends AbstractCommand{

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

}
