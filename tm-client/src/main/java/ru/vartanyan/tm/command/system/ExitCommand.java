package ru.vartanyan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.command.AbstractCommand;

@Component
public class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-e";
    }

    @Override
    public @NotNull String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

}
